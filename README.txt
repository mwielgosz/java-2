Java 2
Full Sail University
Term 1510
Michael Wielgosz

This repository holds all of the projects required for the ‘Java 2’ course at Full Sail University and has been created by Michael Wielgosz.

--------

Git repo: https://bitbucket.org/mwielgosz/java-2
Git clone: git clone git@bitbucket.org:mwielgosz/java-2.git
HTTP clone: git clone https://mwielgosz@bitbucket.org/mwielgosz/java-2.git

--------

Project 1:
Recipe Resource

MIN SDK API: 14
TARGET SDK API: 19
MAX SDK API: 23

Description:
Searches Food2Fork API for recipes based on ingredients and recipe name. Displays results in a list. Selecting a list item shows the recipe details including: title, image, ingredients, full recipe, and publisher.

API URL: http://food2fork.com/about/api


--------

Project 2:
Recipe Resource 1.1

MIN SDK API: 14
TARGET SDK API: 19
MAX SDK API: 23

Description:
Searches Food2Fork API for recipes based on ingredients and recipe name. Displays results in a list. Selecting a list item shows the recipe details including: title, image, ingredients, full recipe, and publisher.

Update 1.1:
Includes two functional and two cosmetic change to app as defined by instructor. See ‘Recipe Resource’ README for more details.

API URL: http://food2fork.com/about/api

--------

Project 3:
Important Contacts

MIN SDK API: 14
TARGET SDK API: 19
MAX SDK API: 23

Description:
‘Important Contacts’ allows users to add, list, and view their most important contacts. Users can enter a variety of contact information and save it to their device or share it with friends. A list of previously saved contacts is displayed and when one is pressed, the full contact details is displayed. Any contact can be deleted while viewing it.