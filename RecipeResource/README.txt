Java 2
Full Sail University
Term: 1510
Project 1 - Fragment & File Fundamentals
Project 2 - Fundamentals 2
Michael Wielgosz

--------

Git repo: https://bitbucket.org/mwielgosz/java-2
Git clone: git clone git@bitbucket.org:mwielgosz/java-2.git
HTTP clone: git clone https://mwielgosz@bitbucket.org/mwielgosz/java-2.git

--------

The included .apk file is a debug release, meaning it must be “side loaded” onto a device. Simply clicking on “app-debug.apk” will install the application. In addition, the code can be cloned from the private git repo (links above) and compiled in Android Studio and tested on a device.

This app was tested on a Nexus 5 running Android 5.1.1 and with the Android Emulator (Nexus S API 14 & Nexus 5 x86 API 21), but is backwards compatible to API 14.

--------

Fundamental 2 Update:

Functional Changes:
- Add a clickable preference to the preference fragment that allows the user to clear out all cached data saved by the application. 
- Add a list preference to the preference fragment that allows the user to change the text color in the app. Provide at least three colors to choose from and apply the selected color to all text in the app. Action bar text does not need to change.

Cosmetic Changes:
- Swap the positioning of your master and detail fragments.
- Change the background for all app components in the master fragment to be blue and all app components in the details fragment to be green.
