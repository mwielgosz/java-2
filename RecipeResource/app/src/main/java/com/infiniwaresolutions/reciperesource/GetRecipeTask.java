package com.infiniwaresolutions.reciperesource;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;


public class GetRecipeTask extends AsyncTask<URL, Integer, Recipe> {

    private String TAG = GetRecipeTask.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    private OnGetRecipeListener mListener;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // Setup progress dialog
        mProgressDialog = new ProgressDialog(MainActivity.mContext);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setProgress(0);
        mProgressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        // Update progress dialog
        mProgressDialog.setProgress(values[0]);
    }

    @Override
    protected Recipe doInBackground(URL... urls) {

        Recipe selectedRecipe = new Recipe();

        for (URL recipeUrl : urls) {
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) recipeUrl.openConnection();
                urlConnection.setRequestMethod("GET");

                urlConnection.setConnectTimeout(10000);
                urlConnection.setReadTimeout(10000);

                int statusCode = urlConnection.getResponseCode();

                if (statusCode == 200) {
                    // Status code OK
                    JSONObject jsonObject = NetworkHelper.streamToJsonObject(urlConnection.getInputStream());
                    urlConnection.disconnect();

                    // Create new Recipe
                    selectedRecipe = new Recipe(jsonObject != null ? jsonObject.getJSONObject("recipe") : null);

                    // Write Recipe to cache
                    FileHelper.writeToRecipeCache(MainActivity.mContext, selectedRecipe);

                } else {
                    // Status code bad
                    urlConnection.disconnect();
                    return null;
                }
            } catch (Exception e) {
                Log.e(TAG, (e.getLocalizedMessage() == null) ? "Network Error" : e.getLocalizedMessage());
                return null;
            }
        }

        return selectedRecipe;
    }

    @Override
    protected void onPostExecute(Recipe result) {
        super.onPostExecute(result);

        // Remove ProgressDialog
        mProgressDialog.dismiss();

        // Ensure Recipe does not equal null
        if (result != null) {
            if (mListener != null) {
                mListener.getRecipe(result);
            }
        } else {
            // Recipe equals null
            AppHelper.createBasicAlertDialog(MainActivity.mContext, "Error", "There was an issue retrieving your recipe. Please try again");
        }
    }

    @Override
    protected void onCancelled(Recipe result) {
        super.onCancelled(result);

        // Remove ProgressDialog & Listener
        mProgressDialog.dismiss();
        mListener = null;
    }

    public void setOnGetRecipeListener(OnGetRecipeListener listener) {
        mListener = listener;
    }

    public interface OnGetRecipeListener {
        public void getRecipe(Recipe recipe);
    }

}
