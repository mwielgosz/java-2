package com.infiniwaresolutions.reciperesource;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

public class RecipeDetailActivity extends Activity {

    private String TAG = RecipeDetailActivity.class.getSimpleName();
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get Context
        mContext = this;

        // Load shared preferences & set text color
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String prefTextColor = sharedPreferences.getString("preference_list_text_color", "0");
        AppHelper.setTextColor(mContext, prefTextColor);
        setContentView(R.layout.activity_recipe_detail);

        // Get Intent
        Intent intent = getIntent();

        // Retrieve Recipe from Intent
        Recipe recipe = (Recipe) intent.getSerializableExtra("recipe");

        // Local temp variables
        String title = "";
        String imageUrl = "";
        String ingredientsArray[] = new String[0];
        String publisher = "";
        String publisherUrl = "";
        String sourceUrl = "";

        if (recipe != null) {
            try {
                // Set Recipe properties
                title = recipe.getTitle();
                imageUrl = recipe.getImageUrl();
                ingredientsArray = recipe.getIngredients();
                publisher = recipe.getPublisher();
                publisherUrl = recipe.getPublisherUrl();
                sourceUrl = recipe.getSourceUrl();
            } catch (Exception e) {
                Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error setting Recipe properties" : e.getLocalizedMessage());
            }
        }

        // Setup View objects
        TextView tvTitleHeader = (TextView) findViewById(R.id.tv_detail_title_header);
        final ImageView ivRecipeImage = (ImageView) findViewById(R.id.iv_detail_image);
        TextView tvIngredients = (TextView) findViewById(R.id.tv_detail_ingredients);
        TextView tvPublisher = (TextView) findViewById(R.id.tv_detail_publisher);
        TextView tvSourceUrl = (TextView) findViewById(R.id.tv_detail_source_url);

        // Set recipe title
        tvTitleHeader.setText(title);

        // Set ImageView with app icon
        ivRecipeImage.setImageResource(R.mipmap.ic_launcher);

        // AsyncTask: retrieve image
        final String finalRecipeImageUrl = imageUrl;
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                if (NetworkHelper.isNetworkConnected(mContext)) {
                    try {
                        // Open InputStream with recipe image URL
                        InputStream inputStream = (InputStream) new URL(finalRecipeImageUrl).getContent();

                        // Decode stream into Bitmap & write to file
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                        FileHelper.writeImageToBitmap(mContext, bitmap);

                        return bitmap;
                    } catch (Exception e) {
                        Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error downloading recipe image" : e.getLocalizedMessage());
                    }
                } else {
                    // Return cached image from last Recipe
                    return FileHelper.readImageFromBitmap(mContext);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null) {
                    // Replace ImageView with recipe image
                    ivRecipeImage.setImageBitmap(bitmap);
                }
            }
        }.execute();

        // Set ingredient list
        String ingredientList = "";
        for (String ingredient : ingredientsArray) {
            ingredientList = ingredientList + "\n" + ingredient;
        }
        tvIngredients.setText(ingredientList);

        // Set source url "See more"
        tvSourceUrl.setText(Html.fromHtml("<a href=" + sourceUrl + ">" + getResources().getString(R.string.link_see_more) + "</a>"));
        tvSourceUrl.setMovementMethod(LinkMovementMethod.getInstance());

        // Set publisher url
        tvPublisher.setText(Html.fromHtml(getResources().getString(R.string.publisher) + " <a href=" + publisherUrl + ">" + publisher + "</a>"));
        tvPublisher.setMovementMethod(LinkMovementMethod.getInstance());
    }

}
