package com.infiniwaresolutions.reciperesource;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class NetworkHelper {

    private static String TAG = NetworkHelper.class.getSimpleName();
    private static String mApiKey = "?key=17dcac046fd3ad2cdf1e7debeaabe851";

    // Returns Boolean if network is connected
    public static Boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isConnected();
        }

        return false;
    }

    // Returns URL for API search
    public static URL getSearchUrl(String userEntry) {
        String apiUrl = "http://food2fork.com/api/search";

        String formattedEntry = userEntry.replace(" ", "%20");

        String concatUrl = apiUrl + mApiKey + "&q=" + formattedEntry;
        URL url = null;
        try {
            url = new URL(concatUrl);
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }

        return url;
    }

    // Return URL of API recipe get
    public static URL getRecipeUrl(String recipeId) {
        String apiUrl = "http://food2fork.com/api/get";

        //String formattedEntry = userEntry.replace(" ", "%20");

        String concatUrl = apiUrl + mApiKey + "&rId=" + recipeId;
        URL url = null;
        try {
            url = new URL(concatUrl);
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }

        return url;
    }

    // Returns JSONObject of InputStream
    public static JSONObject streamToJsonObject(InputStream connectionInputStream) {
        try {
            InputStreamReader inputReader = new InputStreamReader(connectionInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputReader);
            StringBuilder stringBuilder = new StringBuilder();
            String text = null;

            // Loop through & append to stringBuilder
            while ((text = bufferedReader.readLine()) != null) {
                stringBuilder.append(text);
            }

            // Close BufferedReader
            bufferedReader.close();

            return new JSONObject(stringBuilder.toString());
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error building JSONObject from InputStream" : e.getLocalizedMessage());
            return null;
        }
    }

}
