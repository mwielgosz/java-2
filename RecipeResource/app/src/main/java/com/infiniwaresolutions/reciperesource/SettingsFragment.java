package com.infiniwaresolutions.reciperesource;


import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


public class SettingsFragment extends PreferenceFragment {

    private String TAG = SettingsFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set proper background color
        getView().setBackgroundColor(Color.WHITE);


        // Clear cache preference
        Preference prefClearCache = findPreference("preference_clear_cache");
        prefClearCache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference pref) {
                // Clear saved cache files
                Boolean success = FileHelper.deleteCache(MainActivity.mContext);
                if (success) {
                    Toast.makeText(MainActivity.mContext, "Cache cleared", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.mContext, "Error clearing cache", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        // Set text color preference
        ListPreference prefListTextColor = (ListPreference) findPreference("preference_list_text_color");
        prefListTextColor.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                // Save SharedPreferences
                SharedPreferences defaultPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.mContext);
                SharedPreferences.Editor editor = defaultPrefs.edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();

                // Set text color by theme and reload
                AppHelper.setTextColor(MainActivity.mContext, newValue.toString());
                MainActivity.mActivity.recreate();
                return true;
            }
        });
    }
}
