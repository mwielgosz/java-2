package com.infiniwaresolutions.reciperesource;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;

public class AppHelper {

    // Creates AlertDialog with POSITIVE "Ok" button
    public static void createBasicAlertDialog(Context dialogContext, String title, String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(dialogContext).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, dialogContext.getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    // Set background of view
    public static void setViewBackgroundColor(View view, String color) {
        switch (color) {
            case "white":
                view.setBackgroundColor(Color.WHITE);
                break;
            case "black":
                view.setBackgroundColor(Color.BLACK);
            case "green":
                view.setBackgroundColor(Color.GREEN);
                break;
            case "blue":
                view.setBackgroundColor(Color.BLUE);
                break;
        }
    }

    // Set color of text across app by theme
    public static void setTextColor(Context context, String color) {
        switch (color) {
            case "0":
                context.setTheme(R.style.AppTheme);
                break;
            case "1":
                context.setTheme(R.style.AppThemeDarkGrey);
                break;
            case "2":
                context.setTheme(R.style.AppThemeOrange);
                break;
            case "3":
                context.setTheme(R.style.AppThemeRed);
                break;
            default:
                context.setTheme(R.style.AppTheme);
                break;
        }
    }

}
