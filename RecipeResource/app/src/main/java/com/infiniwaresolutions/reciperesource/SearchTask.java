package com.infiniwaresolutions.reciperesource;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class SearchTask extends AsyncTask<URL, Integer, ArrayList<SearchResult>> {

    private String TAG = SearchTask.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    private OnSearchListener mListener;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // Setup progress dialog
        mProgressDialog = new ProgressDialog(MainActivity.mContext);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setProgress(0);
        mProgressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        // Update progress dialog
        mProgressDialog.setProgress(values[0]);
    }

    @Override
    protected ArrayList<SearchResult> doInBackground(URL... urls) {

        ArrayList<SearchResult> recipeSearchArray = new ArrayList<>();

        for (URL searchUrl : urls) {
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) searchUrl.openConnection();
                urlConnection.setRequestMethod("GET");

                urlConnection.setConnectTimeout(10000);
                urlConnection.setReadTimeout(10000);

                int statusCode = urlConnection.getResponseCode();

                if (statusCode == 200) {
                    // Status code OK
                    JSONObject jsonObject = NetworkHelper.streamToJsonObject(urlConnection.getInputStream());
                    urlConnection.disconnect();

                    // Create new SearchResult objects in ArrayList
                    for (int i = 0; (jsonObject != null ? jsonObject.getJSONArray("recipes").length() : 0) > i; i++) {
                        SearchResult searchResult = new SearchResult(jsonObject.getJSONArray("recipes").getJSONObject(i));
                        recipeSearchArray.add(searchResult);
                    }

                    // Write search results to cache
                    FileHelper.writeToSearchCache(MainActivity.mContext, recipeSearchArray);


                } else {
                    urlConnection.disconnect();
                    return null;
                }

            } catch (Exception e) {
                Log.e(TAG, (e.getLocalizedMessage() == null) ? "Network Error" : e.getLocalizedMessage());
                return null;
            }
        }

        return recipeSearchArray;
    }

    @Override
    protected void onPostExecute(ArrayList<SearchResult> result) {
        super.onPostExecute(result);

        // Remove ProgressDialog
        mProgressDialog.dismiss();

        // Ensure ArrayList<SearchResult> does not equal null
        if (result != null) {
            if (mListener != null) {
                mListener.searchRecipes(result);
            }
        } else {
            // ArrayList<SearchResult> equals null
            AppHelper.createBasicAlertDialog(MainActivity.mContext, "Error", "There was an issue retrieving your search results. Please try again");
        }
    }

    @Override
    protected void onCancelled(ArrayList<SearchResult> result) {
        super.onCancelled(result);

        // Remove ProgressDialog & Listener
        mProgressDialog.dismiss();
        mListener = null;
    }

    public void setOnSearchListener(OnSearchListener listener) {
        mListener = listener;
    }

    public interface OnSearchListener {
        public void searchRecipes(ArrayList<SearchResult> searchResults);
    }

}
