package com.infiniwaresolutions.reciperesource;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FileHelper {

    public static String TAG = FileHelper.class.getSimpleName();
    private static final String FILENAME_SEARCH = "search_cache.dat";
    private static final String FILENAME_RECIPE = "recipe_cache.dat";
    private static final String FILEPATH_RECIPE_IMAGE = "recipe_images";
    private static final String FILENAME_RECIPE_IMAGE = "recipe_image.png";

    // Write search results (ArrayList<SearchResult>) to internal cache
    public static void writeToSearchCache(Context context, ArrayList<SearchResult> searchResultArrayList) {

        try {
            FileOutputStream fos = context.openFileOutput(FILENAME_SEARCH, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            // Write ArrayList<SearchResult> object to file
            oos.writeObject(searchResultArrayList);

            // CLose file
            oos.close();
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error writing to cache" : e.getLocalizedMessage());
        }
    }

    // Read search results (ArrayList<SearchResult>) from internal cache
    public static ArrayList<SearchResult> readFromSearchCache(Context context) {
        try {
            FileInputStream fin = context.openFileInput(FILENAME_SEARCH);
            ObjectInputStream oin = new ObjectInputStream(fin);

            // Read ArrayList<SearchResult> object from stream
            ArrayList<SearchResult> cachedDearchResultArrayList = (ArrayList<SearchResult>) oin.readObject();

            // Close stream
            oin.close();

            return cachedDearchResultArrayList;
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error reading from cache" : e.getLocalizedMessage());
        }

        return null;
    }

    // Write recipe (Recipe) to internal cache
    public static void writeToRecipeCache(Context context, Recipe recipe) {
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME_RECIPE, Context.MODE_PRIVATE);

            ObjectOutputStream oos = new ObjectOutputStream(fos);

            // Write Recipe object to file
            oos.writeObject(recipe);

            // Close file
            oos.close();
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error writing to cache" : e.getLocalizedMessage());
        }
    }

    // Read recipe (Recipe) from internal cache
    public static Recipe readFromRecipeCache(Context context) {
        try {
            FileInputStream fin = context.openFileInput(FILENAME_RECIPE);
            ObjectInputStream oin = new ObjectInputStream(fin);

            // Read Recipe object from Stream
            Recipe cachedRecipe = (Recipe) oin.readObject();

            // Close stream
            oin.close();

            return cachedRecipe;
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error reading from cache" : e.getLocalizedMessage());
            return null;
        }
    }

    // Write recipe image (Bitmap) to internal cache
    public static void writeImageToBitmap(Context context, Bitmap recipeImage) {
        // Get file path to load image
        File directory = new File(context.getFilesDir().getPath());
        File path = new File(directory, FILENAME_RECIPE_IMAGE);
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(path);

            // Compress Bitmap to FileOutputStream
            recipeImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error writing recipe image" : e.getLocalizedMessage());
        }
    }

    // Read recipe image (Bitmap) from internal cache
    public static Bitmap readImageFromBitmap(Context context) {
        // Get file path to load image
        File directory = new File(context.getFilesDir().getPath());

        try {
            // Get file path and name & decode Bitmap from FileInputStream
            File file = new File(directory, FILENAME_RECIPE_IMAGE);
            return BitmapFactory.decodeStream(new FileInputStream(file));
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error reading recipe image" : e.getLocalizedMessage());
            return null;
        }
    }


    // Delete cache/saved files
    public static Boolean deleteCache(Context context) {
        File directory = new File(context.getFilesDir().getPath());
        Boolean cacheCleared = false;

        // Loop through all files in directory and delete
        for (File file : directory.listFiles()) {
            cacheCleared = file.delete();
            if (!cacheCleared) {
                break;
            }
        }
        return cacheCleared;
    }

}
