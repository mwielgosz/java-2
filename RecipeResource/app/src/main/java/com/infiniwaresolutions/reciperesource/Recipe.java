package com.infiniwaresolutions.reciperesource;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class Recipe implements Serializable {

    private String TAG = Recipe.class.getSimpleName();
    private static final long serialVersionUID = 002L;

    private String mId;
    private String mTitle;
    private String mPublisher;
    private String mPublisherUrl;
    private String mImageUrl;
    private String[] mIngredients;
    private String mSourceUrl;

    public Recipe() {
    }

    public Recipe(String id, String title, String publisher, String publisherUrl, String imageUrl, String[] ingredients, String sourceUrl) {
        this.mId = id;
        this.mTitle = title;
        this.mPublisher = publisher;
        this.mPublisherUrl = publisherUrl;
        this.mImageUrl = imageUrl;
        this.mIngredients = ingredients;
        this.mSourceUrl = sourceUrl;
    }

    public Recipe(JSONObject searchResultData) {
        try {
            this.mId = searchResultData.getString("recipe_id");
            this.mTitle = searchResultData.getString("title");
            this.mPublisher = searchResultData.getString("publisher");
            this.mPublisherUrl = searchResultData.getString("publisher_url");
            this.mImageUrl = searchResultData.getString("image_url");
            this.mSourceUrl = searchResultData.getString("source_url");

            JSONArray ingredientArray = searchResultData.getJSONArray("ingredients");
            mIngredients = new String[ingredientArray.length()];

            for (int i = 0; ingredientArray.length() > i; i++) {
                this.mIngredients[i] = ingredientArray.getString(i);
            }
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error storing JSONObject in Recipe" : e.getLocalizedMessage());
        }
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setPublisher(String publisher) {
        this.mPublisher = publisher;
    }

    public String getPublisher() {
        return mPublisher;
    }

    public void setPublisherUrl(String publisherUrl) {
        this.mPublisherUrl = publisherUrl;
    }

    public String getPublisherUrl() {
        return mPublisherUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setIngredients(String[] ingredients) {
        this.mIngredients = ingredients;
    }

    public String[] getIngredients() {
        return mIngredients;
    }

    public void setSourceUrl(String sourceUrl) {
        this.mSourceUrl = sourceUrl;
    }

    public String getSourceUrl() {
        return mSourceUrl;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
