package com.infiniwaresolutions.reciperesource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;

import java.net.URL;

public class MainActivity extends Activity implements ResultsFragment.OnResultClickListener {

    private String TAG = MainActivity.class.getSimpleName();
    public static Context mContext;
    public static Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set context & activity
        mContext = this;
        mActivity = this;

        // Load shared preferences & set text color
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String prefTextColor = sharedPreferences.getString("preference_list_text_color", "0");
        AppHelper.setTextColor(mContext, prefTextColor);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null) {
            Fragment fragment = new SearchFragment().newInstance(null);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frag_master_container, fragment, SearchFragment.TAG).commit();

            fragment = new ResultsFragment();
            fragmentManager.beginTransaction().replace(R.id.frag_results_container, fragment, ResultsFragment.TAG).commit();
        }
    }

    @Override
    public void onSearchResultClick(String id) {

        // Ensure 'id' is not empty
        if (!id.equals("")) {
            // Ensure device has network connectivity
            if (NetworkHelper.isNetworkConnected(mContext)) {
                URL getRecipeUrl = NetworkHelper.getRecipeUrl(id);

                // Start SearchTask (AsyncTask) for API data traversing
                GetRecipeTask getRecipeTask = new GetRecipeTask();
                getRecipeTask.setOnGetRecipeListener(new GetRecipeTask.OnGetRecipeListener() {
                    @Override
                    public void getRecipe(Recipe recipe) {
                        // Launch RecipeDetailActivity if there are results
                        if (recipe != null) {
                            Intent recipeDetails = new Intent(mContext, RecipeDetailActivity.class);
                            recipeDetails.putExtra("recipe", recipe);

                            startActivity(recipeDetails);
                        } else {
                            AppHelper.createBasicAlertDialog(mContext, "Recipe Error", "There was an issue loading your recipe. Please try again");
                        }
                    }
                });
                getRecipeTask.execute(getRecipeUrl);

            } else {
                // No network connection: Show AlertDialog for user interactivity
                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(mContext).create();
                alertDialog.setTitle(getResources().getString(R.string.error_title_no_network_connection));
                alertDialog.setMessage(getResources().getString(R.string.error_recipe_message_no_network_connection));
                alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, mContext.getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // 'Ok'/POSITIVE pressed
                                dialog.dismiss();

                                // Read last viewed recipe from cache
                                Recipe recipe = FileHelper.readFromRecipeCache(mContext);

                                if (recipe != null) {
                                    // Setup & launch Intent for RecipeDetailActivity
                                    Intent recipeDetails = new Intent(mContext, RecipeDetailActivity.class);
                                    recipeDetails.putExtra("recipe", recipe);
                                    startActivity(recipeDetails);
                                } else {
                                    AppHelper.createBasicAlertDialog(mContext, "Recipe Error", "No previously viewed recipes have been found. Please establish a network connection and try again");
                                }
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, MainActivity.mContext.getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 'Cancel'/NEGATIVE pressed
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        } else {
            AppHelper.createBasicAlertDialog(mContext, "Recipe Error", "The recipe you selected was not found");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the Settings menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            // Open SettingsFragment on menu click
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStack();
            fragmentManager.beginTransaction().replace(android.R.id.content, new SettingsFragment()).addToBackStack(null).commit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
