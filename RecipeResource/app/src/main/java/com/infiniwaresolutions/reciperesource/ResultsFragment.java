package com.infiniwaresolutions.reciperesource;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class ResultsFragment extends ListFragment {

    public static String TAG = ResultsFragment.class.getSimpleName();
    private static final String ARG_PARAM_RECIPE_SEARCH_RESULTS = "param_recipe_search_results";
    private static final String CONSTANT_BG_COLOR = "green";
    private OnResultClickListener mListener;
    private ArrayList<SearchResult> mParamRecipeResults;

    public static ResultsFragment newInstance(ArrayList<SearchResult> searchResultArray) {
        ResultsFragment fragment = new ResultsFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM_RECIPE_SEARCH_RESULTS, searchResultArray);
        fragment.setArguments(args);

        return fragment;
    }

    public ResultsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParamRecipeResults = (ArrayList<SearchResult>) getArguments().getSerializable(ARG_PARAM_RECIPE_SEARCH_RESULTS);
            // Setup basic list adapter for displaying results
            setListAdapter(new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, mParamRecipeResults));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Only show list results (no loading progress indicator)
        setListShown(true);

        // Set when ListFragment is empty
        setEmptyText(getResources().getString(R.string.hint_empty_list));

        AppHelper.setViewBackgroundColor(getView(), CONSTANT_BG_COLOR);
        AppHelper.setViewBackgroundColor(getListView(), CONSTANT_BG_COLOR);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnResultClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnResultClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            // Pass Recipe to Listener
            mListener.onSearchResultClick(mParamRecipeResults.get(position).getId());
        }
    }

    // Listener interface
    public interface OnResultClickListener {
        public void onSearchResultClick(String id);
    }

}
