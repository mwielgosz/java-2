package com.infiniwaresolutions.reciperesource;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;


public class SearchFragment extends Fragment {

    public static String TAG = SearchFragment.class.getSimpleName();
    private static final String ARG_PARAM_SEARCH_QUERY = "param_search_query";
    private static final String CONSTANT_BG_COLOR = "blue";
    EditText mEtSearch;
    Button mBtnSearch;

    public static SearchFragment newInstance(String searchQuery) {
        SearchFragment fragment = new SearchFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PARAM_SEARCH_QUERY, searchQuery);
        fragment.setArguments(args);

        return fragment;
    }

    public SearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AppHelper.setViewBackgroundColor(getView(), CONSTANT_BG_COLOR);

        mEtSearch = (EditText) getView().findViewById(R.id.et_user_search);
        mBtnSearch = (Button) getView().findViewById(R.id.btn_search);

        AppHelper.setViewBackgroundColor(mEtSearch, CONSTANT_BG_COLOR);
        AppHelper.setViewBackgroundColor(mBtnSearch, CONSTANT_BG_COLOR);

        if (savedInstanceState != null) {
            mEtSearch.setText(savedInstanceState.getString(ARG_PARAM_SEARCH_QUERY));
        }

        // "Search" Button onClickListener
        mBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss keyboard & start search on keyboard "Enter" press
                InputMethodManager imm = (InputMethodManager) MainActivity.mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                startSearch(mEtSearch.getText().toString());
            }
        });

        // "Search" EditText onEditorActionListener
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Dismiss keyboard & start search on keyboard "Enter" press
                    InputMethodManager imm = (InputMethodManager) MainActivity.mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    startSearch(mEtSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    private void startSearch(String searchQuery) {
        // Ensure 'Search' EditText has text
        if (!searchQuery.equals("")) {

            // Ensure device has network connectivity
            if (NetworkHelper.isNetworkConnected(MainActivity.mContext)) {

                // Search clicked - start SearchTask 'GET' data traverse
                URL searchUrl = NetworkHelper.getSearchUrl(searchQuery);

                // Start SearchTask (AsyncTask) for API data traversing
                SearchTask searchTask = new SearchTask();
                searchTask.setOnSearchListener(new SearchTask.OnSearchListener() {
                    @Override
                    public void searchRecipes(ArrayList<SearchResult> searchResults) {
                        // Listener callback for when AsyncTask completes
                        Fragment fragment = new ResultsFragment().newInstance(searchResults);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frag_results_container, fragment, ResultsFragment.TAG).commit();
                    }
                });
                searchTask.execute(searchUrl);

            } else {
                // No network connection: Show AlertDialog for user interactivity
                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MainActivity.mContext).create();
                alertDialog.setTitle(getResources().getString(R.string.error_title_no_network_connection));
                alertDialog.setMessage(getResources().getString(R.string.error_search_message_no_network_connection));
                alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, MainActivity.mContext.getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // 'Ok'/POSITIVE pressed
                                dialog.dismiss();

                                // Read last search results from cache
                                ArrayList<SearchResult> searchResults;
                                searchResults = FileHelper.readFromSearchCache(MainActivity.mContext);

                                if (searchResults != null) {
                                    Fragment fragment = new ResultsFragment().newInstance(searchResults);
                                    FragmentManager fragmentManager = getFragmentManager();
                                    fragmentManager.beginTransaction().replace(R.id.frag_results_container, fragment, ResultsFragment.TAG).commit();
                                } else {
                                    AppHelper.createBasicAlertDialog(MainActivity.mContext, "Search Error", "No previous searches have been found. Please establish a network connection and try again");
                                }
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, MainActivity.mContext.getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 'Cancel'/NEGATIVE pressed
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        } else {
            // No text in 'Search' EditText
            AppHelper.createBasicAlertDialog(MainActivity.mContext, "Error", "Please enter a search term");
        }
    }


}
