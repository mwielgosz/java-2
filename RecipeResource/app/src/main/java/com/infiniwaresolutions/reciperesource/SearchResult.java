package com.infiniwaresolutions.reciperesource;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class SearchResult implements Serializable {

    private String TAG = SearchResult.class.getSimpleName();
    private static final long serialVersionUID = 001L;

    private String mId;
    private String mTitle;
    private String mPublisher;
    private String mImageUrl;

    public SearchResult() {
    }

    public SearchResult(String id, String title, String publisher, String imageUrl) {
        this.mId = id;
        this.mTitle = title;
        this.mPublisher = publisher;
        this.mImageUrl = imageUrl;
    }

    public SearchResult(JSONObject searchResultData) {
        try {
            this.mId = searchResultData.getString("recipe_id");
            this.mTitle = searchResultData.getString("title");
            this.mPublisher = searchResultData.getString("publisher");
            this.mImageUrl = searchResultData.getString("image_url");
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error storing JSONObject into SearchResult" : e.getLocalizedMessage());
        }
    }

    public SearchResult(JSONArray searchResultArray) {
        try {
            for (int i = 0; searchResultArray.length() > 0; i++) {
                this.mId = searchResultArray.getJSONObject(i).getString("recipe_id");
                this.mTitle = searchResultArray.getJSONObject(i).getString("title");
                this.mPublisher = searchResultArray.getJSONObject(i).getString("publisher");
                this.mImageUrl = searchResultArray.getJSONObject(i).getString("image_url");
            }
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error storing JSONObject into SearchResult" : e.getLocalizedMessage());
        }
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setPublisher(String publisher) {
        this.mPublisher = publisher;
    }

    public String getPublisher() {
        return mPublisher;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    @Override
    public String toString() {
        return mTitle;
    }

}
