package com.infiniwaresolutions.importantcontactsprovider;


public class Contract {
    public static final String CONTENT_URI = "content://com.infiniwaresolutions.importantcontactsprovider/";
    public static final String DATA_SOURCE = "contact_table";
    public static final String DATA_SOURCE_URI = CONTENT_URI + DATA_SOURCE;

    // Column names
    public static final String _ID = "_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String PHYSICAL_ADDRESS = "physical_address";
    public static final String DETAILS = "details";

    public static final int MATCH_FIRST_NAME = 1;
    public static final int MATCH_LAST_NAME = 2;
    public static final int MATCH_PHONE_NUMBER = 3;
    public static final int MATCH_EMAIL_ADDRESS = 4;
    public static final int MATCH_PHYSICAL_ADDRESS = 5;
    public static final int MATCH_DETAILS = 6;
}
