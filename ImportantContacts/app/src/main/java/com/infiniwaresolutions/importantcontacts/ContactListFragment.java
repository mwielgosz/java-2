package com.infiniwaresolutions.importantcontacts;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class ContactListFragment extends ListFragment {

    private String TAG = ContactListFragment.class.getSimpleName();
    private static final String ARG_PARAM_CONTACT_ARRAY = "param_contact_array";
    private ArrayList<ContactItem> mParamContactArray;
    private OnContactListTouchListener mListener;

    public static ContactListFragment newInstance(ArrayList<ContactItem> paramContactArray) {
        ContactListFragment fragment = new ContactListFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM_CONTACT_ARRAY, paramContactArray);
        fragment.setArguments(args);

        return fragment;
    }

    public ContactListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParamContactArray = (ArrayList<ContactItem>) getArguments().getSerializable(ARG_PARAM_CONTACT_ARRAY);

            setListAdapter(new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, mParamContactArray));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Display list properly when empty
        setListShown(true);
        setEmptyText(getResources().getString(R.string.hint_empty_list));
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnContactListTouchListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnContactListTouchListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            mListener.onContactListTouch(mParamContactArray.get(position).getId());
        }
    }

    public interface OnContactListTouchListener {
        public void onContactListTouch(int id);
    }

}
