package com.infiniwaresolutions.importantcontacts;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends Activity implements ContactListFragment.OnContactListTouchListener {

    private String TAG = MainActivity.class.getSimpleName();
    public static String ARG_CONTACT = "arg_contact";
    public static ArrayList<ContactItem> mContactItemArrayList = new ArrayList<>();
    public static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        // Get contacts from cache & load into ListFragment
        getContactsForListFragment();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        // Get contacts from cache & load into ListFragment
        getContactsForListFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add_new_contact:
                // Add new contact
                Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onContactListTouch(int id) {
        // Start View Contact activity
        Intent intent = new Intent(mContext, ViewContactActivity.class);
        intent.putExtra(ARG_CONTACT, mContactItemArrayList.get(id));
        startActivity(intent);
    }

    private void getContactsForListFragment() {
        // Read contacts from cache
        mContactItemArrayList = FileHelper.readContactsFromCache(mContext);

        // Show ListFragment
        Fragment fragment = new ContactListFragment().newInstance(mContactItemArrayList);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
    }

}
