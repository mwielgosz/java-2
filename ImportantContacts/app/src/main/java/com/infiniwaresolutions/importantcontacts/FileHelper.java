package com.infiniwaresolutions.importantcontacts;


import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FileHelper {

    public static String TAG = FileHelper.class.getSimpleName();
    private static final String FILENAME_CONTACTS = "contacts_cache.dat";

    // Write contacts to internal cache
    public static void writeContactsToCache(Context context, ArrayList<ContactItem> contactItems) {
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME_CONTACTS, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            // Write ArrayList<ContactItem> object to file
            oos.writeObject(contactItems);

            // CLose file
            oos.close();
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error writing to cache" : e.getLocalizedMessage());
        }
    }

    // Read contacts from internal cache
    public static ArrayList<ContactItem> readContactsFromCache(Context context) {
        try {
            // Get file list
            String[] contextFileList = context.fileList();

            // Ensure files exist in contextFileList
            for (String fileList : contextFileList) {
                if (fileList.contains(FILENAME_CONTACTS)) {
                    FileInputStream fin = context.openFileInput(FILENAME_CONTACTS);
                    ObjectInputStream oin = new ObjectInputStream(fin);

                    // Read ArrayList<ContactItem> object from stream
                    ArrayList<ContactItem> cachedContactItems = (ArrayList<ContactItem>) oin.readObject();

                    // Close stream
                    oin.close();

                    return cachedContactItems;
                } else {
                    return new ArrayList<>();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, (e.getLocalizedMessage() == null) ? "Error reading from cache" : e.getLocalizedMessage());
        }
        return new ArrayList<>();
    }

}
