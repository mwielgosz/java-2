package com.infiniwaresolutions.importantcontacts;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


public class AddContactFragment extends Fragment {

    private String TAG = AddContactFragment.class.getSimpleName();
    private EditText mEtFirstName;
    private EditText mEtLastName;
    private EditText mEtPhoneNumber;
    private EditText mEtEmailAddress;
    private EditText mEtPhysicalAddress;
    private EditText mEtDetails;

    public static AddContactFragment newInstance() {
        return new AddContactFragment();
    }

    public static AddContactFragment newInstance(ContactItem contactItem) {
        AddContactFragment fragment = new AddContactFragment();

        Bundle args = new Bundle();
        args.putSerializable(MainActivity.ARG_CONTACT, contactItem);
        fragment.setArguments(args);

        return fragment;
    }

    public AddContactFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Allow Fragment to control Options Menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_contact, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Find all child views by ID
        mEtFirstName = (EditText) getView().findViewById(R.id.et_name_first);
        mEtLastName = (EditText) getView().findViewById(R.id.et_name_last);
        mEtPhoneNumber = (EditText) getView().findViewById(R.id.et_phone_number);
        mEtEmailAddress = (EditText) getView().findViewById(R.id.et_email_address);
        mEtPhysicalAddress = (EditText) getView().findViewById(R.id.et_physical_address);
        mEtDetails = (EditText) getView().findViewById(R.id.et_details);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                // Return to previous activity
                getActivity().finish();
                return true;
            case R.id.action_save_contact:
                // Save contact
                int id = -1;
                String firstName = mEtFirstName.getText().toString();
                String lastName = mEtLastName.getText().toString();
                String phoneNumber = mEtPhoneNumber.getText().toString();
                String emailAddress = mEtEmailAddress.getText().toString();
                String physicalAddress = mEtPhysicalAddress.getText().toString();
                String details = mEtDetails.getText().toString();

                if (!firstName.equals("") && !lastName.equals("")) {
                    // New contact
                    id = MainActivity.mContactItemArrayList.size();
                    ContactItem contactItem = new ContactItem(id, firstName, lastName, phoneNumber, emailAddress, physicalAddress, details);
                    MainActivity.mContactItemArrayList.add(contactItem);

                    // Write ArrayList<ContactItem> to file
                    FileHelper.writeContactsToCache(getActivity().getApplicationContext(), MainActivity.mContactItemArrayList);

                    // Finish activity
                    finishAddEditActivity(contactItem);
                    return true;
                } else {
                    Toast.makeText(MainActivity.mContext, "First & Last name are required", Toast.LENGTH_LONG).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Finish AddEditActivity
    private void finishAddEditActivity(ContactItem savedContactItem) {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.ARG_CONTACT, savedContactItem);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

}
