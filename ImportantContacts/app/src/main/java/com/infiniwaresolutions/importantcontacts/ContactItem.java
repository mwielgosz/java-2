package com.infiniwaresolutions.importantcontacts;

import java.io.Serializable;

public class ContactItem implements Serializable {

    private String TAG = ContactItem.class.getSimpleName();
    private static final long serialVersionUID = 100L;

    private int mId;
    private String mFirstName;
    private String mLastName;
    private String mPhoneNumber;
    private String mEmailAddress;
    private String mPhysicalAddress;
    private String mDetails;

    public ContactItem(int id, String firstName, String lastName, String phoneNumber, String emailAddress, String physicalAddress, String details) {
        this.mId = id;
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mPhoneNumber = phoneNumber;
        this.mEmailAddress = emailAddress;
        this.mPhysicalAddress = physicalAddress;
        this.mDetails = details;
    }

    public int getId() {
        return this.mId;
    }

    public String getFirstName() {
        return this.mFirstName;
    }

    public String getLastName() {
        return this.mLastName;
    }

    public String getPhoneNumber() {
        return this.mPhoneNumber;
    }

    public String getEmailAddress() {
        return this.mEmailAddress;
    }

    public String getPhysicalAddress() {
        return this.mPhysicalAddress;
    }

    public String getDetails() {
        return this.mDetails;
    }

    @Override
    public String toString() {
        return mFirstName + " " + mLastName;
    }

}
