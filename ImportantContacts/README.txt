Java 2
Full Sail University
Term: 1510
Project 3 - Multi-Activity App
Michael Wielgosz

--------

Git repo: https://bitbucket.org/mwielgosz/java-2
Git clone: git clone git@bitbucket.org:mwielgosz/java-2.git
HTTP clone: git clone https://mwielgosz@bitbucket.org/mwielgosz/java-2.git

--------

The included .apk file is a debug release, meaning it must be “side loaded” onto a device. Simply clicking on “app-debug.apk” will install the application. In addition, the code can be cloned from the private git repo (links above) and compiled in Android Studio and tested on a device.

This app was tested on a Nexus 5 running Android 5.1.1 and with the Android Emulator (Nexus S API 14), but is backwards compatible to API 14.

--------

Description:
‘Important Contacts’ allows users to add, list, and view their most important contacts. Users can enter a variety of contact information and save it to their device or share it with friends. A list of previously saved contacts is displayed and when one is pressed, the full contact details is displayed. Any contact can be deleted while viewing it.