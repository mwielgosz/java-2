package com.infiniwaresolutions.importantcontacts2;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

public class AddContactActivity extends Activity {

    private String TAG = AddContactActivity.class.getSimpleName();
    public static final int ADD_REQUESTCODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Fragment fragment = new AddContactFragment().newInstance();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_add, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
