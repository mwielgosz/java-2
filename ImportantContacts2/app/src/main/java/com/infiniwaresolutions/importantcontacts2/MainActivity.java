package com.infiniwaresolutions.importantcontacts2;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.infiniwaresolutions.importantcontacts2.provider.ProviderContract;

import java.util.ArrayList;

public class MainActivity extends Activity implements ContactListFragment.OnContactListTouchListener {

    private String TAG = MainActivity.class.getSimpleName();
    public static String ARG_CONTACT = "arg_contact";
    public static ArrayList<ContactItem> mContactItemArrayList = new ArrayList<>();
    public static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        // Get contacts from cache & load into ListFragment
        getContactsForListFragment();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        // Get contacts from cache & load into ListFragment
        getContactsForListFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add_new_contact:
                // Add new contact
                Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onContactListTouch(int id) {
        // Start View Contact activity
        Intent intent = new Intent(mContext, ViewContactActivity.class);
        intent.putExtra(ARG_CONTACT, mContactItemArrayList.get(id));
        startActivity(intent);
    }

    private void getContactsForListFragment() {
        // Retrieve contacts
        String URL = ProviderContract.DATA_SOURCE_URI_STRING;

        Uri uri = Uri.parse(URL);

        mContactItemArrayList.clear();

        Cursor cursor = getContentResolver().query(uri, null, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                ContactItem contactItem = new ContactItem(cursor.getString(cursor.getColumnIndex(ProviderContract._ID)),
                        cursor.getString(cursor.getColumnIndex(ProviderContract.FIRST_NAME)),
                        cursor.getString(cursor.getColumnIndex(ProviderContract.LAST_NAME)),
                        cursor.getString(cursor.getColumnIndex(ProviderContract.PHONE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ProviderContract.EMAIL_ADDRESS)),
                        cursor.getString(cursor.getColumnIndex(ProviderContract.PHYSICAL_ADDRESS)),
                        cursor.getString(cursor.getColumnIndex(ProviderContract.DETAILS)));

                mContactItemArrayList.add(contactItem);
            }
        }

        // Show ListFragment
        Fragment fragment = new ContactListFragment().newInstance(mContactItemArrayList);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
    }
}
