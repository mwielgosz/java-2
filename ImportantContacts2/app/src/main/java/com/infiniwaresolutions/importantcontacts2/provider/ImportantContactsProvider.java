package com.infiniwaresolutions.importantcontacts2.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;


public class ImportantContactsProvider extends ContentProvider {

    private static final String TAG = ImportantContactsProvider.class.getSimpleName();

    private SQLiteOpenHelper mDbHelper;

    private static UriMatcher mUriMatcher;

    private SQLiteDatabase mDatabase;
    static final String DATABASE_NAME = "Contacts";
    static final String TABLE_NAME = "contactsTable";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ProviderContract.FIRST_NAME + " TEXT NOT NULL, " +
                    ProviderContract.LAST_NAME + " TEXT NOT NULL, " +
                    ProviderContract.PHONE_NUMBER + " TEXT, " +
                    ProviderContract.EMAIL_ADDRESS + " TEXT, " +
                    ProviderContract.PHYSICAL_ADDRESS + " TEXT, " +
                    ProviderContract.DETAILS + " TEXT);";

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public  DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.i(TAG, "Upgrading database from " + oldVersion + " to " + newVersion);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(ProviderContract.CONTENT_URI, TABLE_NAME, ProviderContract.MATCH_ID);

        mDbHelper = new DatabaseHelper(context);

        mDatabase = mDbHelper.getWritableDatabase();

        return mDatabase != null;

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(TABLE_NAME);

        Cursor cursor = queryBuilder.query(mDatabase, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long row = mDatabase.insert(TABLE_NAME, "", contentValues);

        if (row > 0) {
            Uri insertUri = ContentUris.withAppendedId(ProviderContract.DATA_SOURCE_URI, row);
            getContext().getContentResolver().notifyChange(insertUri, null);
            return insertUri;
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;

        mDatabase.delete(TABLE_NAME, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;

        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (mUriMatcher.match(uri)) {
            case ProviderContract.MATCH_ID:
                return "vnd.android.cursor.item/vnd.com.infiniwaresolutions.provider.Contacts." + TABLE_NAME;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

}
