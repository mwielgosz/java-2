package com.infiniwaresolutions.importantcontacts2;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.infiniwaresolutions.importantcontacts2.provider.ProviderContract;


public class ViewContactFragment extends Fragment {

    private String TAG = ViewContactFragment.class.getSimpleName();
    private ContactItem mContactItem;


    public static ViewContactFragment newInstance(ContactItem contactItem) {
        ViewContactFragment fragment = new ViewContactFragment();

        Bundle args = new Bundle();
        args.putSerializable(MainActivity.ARG_CONTACT, contactItem);
        fragment.setArguments(args);

        return fragment;
    }

    public ViewContactFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Allow Fragment to control Options Menu
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mContactItem = (ContactItem) getArguments().getSerializable(MainActivity.ARG_CONTACT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_contact, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Find all child views by ID
        TextView tvFirstName = (TextView) getView().findViewById(R.id.tv_name_first);
        TextView tvLastName = (TextView) getView().findViewById(R.id.tv_name_last);
        TextView tvPhoneNumber = (TextView) getView().findViewById(R.id.tv_phone_number);
        TextView tvEmailAddress = (TextView) getView().findViewById(R.id.tv_email_address);
        TextView tvPhysicalAddress = (TextView) getView().findViewById(R.id.tv_physical_address);
        TextView tvDetails = (TextView) getView().findViewById(R.id.tv_details);

        if (mContactItem != null) {
            // Set text of child views to stored contact values
            tvFirstName.setText(mContactItem.getFirstName());
            tvLastName.setText(mContactItem.getLastName());
            tvPhoneNumber.setText(mContactItem.getPhoneNumber());
            tvEmailAddress.setText(mContactItem.getEmailAddress());
            tvPhysicalAddress.setText(mContactItem.getPhysicalAddress());
            tvDetails.setText(mContactItem.getDetails());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == AddContactActivity.ADD_REQUESTCODE) {
            ContactItem contactItem = (ContactItem) data.getSerializableExtra(MainActivity.ARG_CONTACT);
            Fragment fragment = ViewContactFragment.newInstance(contactItem);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_view, fragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                // Return to previous activity
                getActivity().finish();
                return true;
            case R.id.action_share_contact:
                // Share contact name with user selected app
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "I know " + mContactItem.getFirstName() + " " + mContactItem.getLastName());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share important contact with..."));
                return true;
            case R.id.action_delete_contact:
                // Delete Contact
                String whereClause = ProviderContract._ID + "=?";
                String[] whereArgs = new String[] { mContactItem.getId() };

                MainActivity.mContext.getContentResolver()
                        .delete(ProviderContract.DATA_SOURCE_URI, whereClause, whereArgs);

                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
