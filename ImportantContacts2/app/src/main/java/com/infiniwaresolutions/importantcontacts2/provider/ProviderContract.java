package com.infiniwaresolutions.importantcontacts2.provider;


import android.net.Uri;

public class ProviderContract {
    public static final String CONTENT_URI = "content://com.infiniwaresolutions.provider.Contacts/";
    public static final String DATA_SOURCE = "contact_table";
    public static final String DATA_SOURCE_URI_STRING = CONTENT_URI + DATA_SOURCE;
    public static final Uri DATA_SOURCE_URI = Uri.parse(DATA_SOURCE_URI_STRING);

    // Column names
    public static final String _ID = "_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String PHYSICAL_ADDRESS = "physical_address";
    public static final String DETAILS = "details";

    public static final int MATCH_ID = 0;
}
