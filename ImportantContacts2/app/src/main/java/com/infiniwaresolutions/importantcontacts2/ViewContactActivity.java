package com.infiniwaresolutions.importantcontacts2;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

public class ViewContactActivity extends Activity {

    private String TAG = ViewContactActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Fragment fragment = new ViewContactFragment().newInstance((ContactItem) intent.getSerializableExtra(MainActivity.ARG_CONTACT));
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_view, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
